using System.Collections;
using UnityEngine;

public class SceneManager : MonoBehaviour
{
    [SerializeField] private string selectableTag = "Selectable";
    [SerializeField] private Material highlightMaterial;

    private GameObject selected = null;
    private GameObject oldSelected = null;

    private void Start()
    {
        StartCoroutine(HighlightObject());
    }

    private IEnumerator HighlightObject()
    {
        while (true)
        {
            //change the color to the highlight
            if (Input.GetMouseButtonDown(0))
            {
                //if selected set to highlight color
                var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, 300.0f))
                {
                    selected = hit.transform.gameObject;
                    if (selected.CompareTag(selectableTag))
                    {
                        //newSelection
                        if (selected != oldSelected)
                        {
                            //Set original material to oldSelected if it's not the first selection
                            DeSelectObject();
                        
                            Material[] newMaterialsArray = new Material[(selected.GetComponent<Renderer>().materials.Length + 1)];
                            selected.GetComponent<Renderer>().materials.CopyTo(newMaterialsArray, 0);
                            newMaterialsArray[^1] = highlightMaterial;
                            selected.GetComponent<Renderer>().materials = newMaterialsArray;

                            //assign oldSelected GO to your current selection
                            oldSelected = selected;
                        }
                    }
                    else
                    {
                        DeSelectObject();
                    }
                }
                else
                {
                    DeSelectObject();
                }
            }
            yield return null;
        }
    }

    private void DeSelectObject()
    {
        if (oldSelected != null)
        {
            Material[] materialsArray = new Material[(oldSelected.GetComponent<Renderer>().materials.Length - 1)];
            for (int i = 0; i < oldSelected.GetComponent<Renderer>().materials.Length - 1; i++)
            {
                materialsArray[i] = oldSelected.GetComponent<Renderer>().materials[i];
            }

            oldSelected.GetComponent<Renderer>().materials = materialsArray;
            oldSelected = null;
        }
    }
}