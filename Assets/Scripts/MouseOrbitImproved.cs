﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[AddComponentMenu("Camera-Control/Mouse Orbit with zoom")]
public class MouseOrbitImproved : MonoBehaviour
{
    public static Action<string> OnShowGraph;
    public bool isRotate;

    [SerializeField] private float _distance = 5.0f;
    [SerializeField] private float _maxDistance = 20;
    [SerializeField] private float _minDistance = 0.6f;
    [SerializeField] private float _panSpeed = 0.3f;
    [SerializeField] private float _zoomDampening = 5.0f;

    [SerializeField] private int _yMinLimit = -80;
    [SerializeField] private int _yMaxLimit = 80;
    [SerializeField] private int _zoomRate = 40;

    [SerializeField] private Vector2 _speed = Vector2.one * 200f;
    [SerializeField] private RectTransform _markerCanvas;
    [SerializeField] private Transform[] _assetsTransform;


    private Transform _target;
    private int _targetIndex = -1;

    private float _xDeg = 0.0f;
    private float _yDeg = 0.0f;
    private float _currentDistance;
    private float _desiredDistance;

    private Quaternion _desiredRotation;
    private Quaternion _rotation;

    private Vector3 _position;
    private RectTransform[] _structureMarkersRect;

    //Camera functions
    private float _pinchScale = 0;
    private Vector3 _worldDelta;
    private Vector3 _prevMousePos;

    //for zooming
    private Vector3 _toTargetPos;
    private bool _isPositionChanging = false;
    
    //For Select Object
    public static string selectedObject;
    public string internalObject;
    public RaycastHit theObject;

    private void Start()
    {        
        GameObject go = new GameObject("CameraTarget");
        go.transform.position = Vector3.zero;
        _toTargetPos = go.transform.position;
        _target = go.transform;
        Initialised();
    }

    public void Initialised()
    {
        //If there is no target, create a temporary target at 'distance' from the cameras current viewpoint
        _currentDistance = _distance;
        _desiredDistance = _distance;

        //be sure to grab the current rotations as starting points.
        _position = transform.position;
        _rotation = transform.rotation;
        _desiredRotation = transform.rotation;

        _xDeg = Vector3.Angle(Vector3.right, transform.right);
        _yDeg = Vector3.Angle(Vector3.up, transform.up);
    }

    private void Update()
    {
        // if (EventSystem.current.IsPointerOverGameObject()  || IsBeingDragged())
        //     return;
        
        _pinchScale = Input.mouseScrollDelta.y * _zoomRate;

        if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1) || Input.GetMouseButtonDown(2))
        {
            _prevMousePos = Input.mousePosition;
            HideShowChart();
        }

        if (Input.GetMouseButton(0) || Input.GetMouseButton(1) || Input.GetMouseButtonDown(2))
        {
            _worldDelta = Input.mousePosition - _prevMousePos;
            _prevMousePos = Input.mousePosition;
        }
    }

    /*
     * Camera logic on LateUpdate to only update after all character movement logic has been handled. 
     */
    private void LateUpdate()
    {
        // if (EventSystem.current.IsPointerOverGameObject() || IsBeingDragged())
        //     return;
        if (_isPositionChanging)
        {
            transform.position = Vector3.Lerp(transform.position, _toTargetPos, Time.deltaTime * 4f);

            if ((_toTargetPos - transform.position).sqrMagnitude <= 0.1f * 0.1f)
            {
                _toTargetPos = _target.position;
                _isPositionChanging = false;
            }

            return;
        }

        UpdateUserMovement();
        
        if(Input.GetMouseButton(1))
            RotationControl(_worldDelta);

        // For smoothing of the zoom, lerp distance
        _currentDistance = Mathf.Lerp(_currentDistance, _desiredDistance, Time.deltaTime * _zoomDampening);
        _target.position = Vector3.Lerp(_target.position, _toTargetPos, Time.deltaTime * 10f);

        // calculate position based on the new currentDistance
        _position = _target.position - (_rotation * Vector3.forward * _currentDistance);

        if (transform.position != _position)
        {
            transform.position = _position;
        }
    }

    private void UpdateUserMovement()
    {
        // If Control and Alt and Middle button? ZOOM!
        // for pinch to zoom with two finger
        
        ZoomControl(_pinchScale * 0.5f);

        ////for Panning
        if (Input.GetMouseButton(0) && !isRotate && (Mathf.Abs(_worldDelta.x) > 0.2f || Mathf.Abs(_worldDelta.y) > 0.2f))
        {
            PanningControl(_worldDelta, _panSpeed);
            return;
        }

        // If middle mouse are selected? ORBIT for rotaion
        if (Input.GetMouseButton(0) && isRotate && (Mathf.Abs(_worldDelta.x) > 0.0f || Mathf.Abs(_worldDelta.y) > 0.0f))
        {
            RotationControl(_worldDelta);
        }
    }

    public void RotationControl(Vector3 _worldDelta)
    {
        if (Mathf.Abs(_worldDelta.x) > Mathf.Abs(_worldDelta.y))
            _xDeg += _worldDelta.x * _speed.x * 0.02f;
        else
            _yDeg -= _worldDelta.y * _speed.y * 0.02f;

        ////////OrbitAngle
        //Clamp the vertical axis for the orbit
        _yDeg = ClampAngle(_yDeg, _yMinLimit, _yMaxLimit);
        // set camera rotation 
        _desiredRotation = Quaternion.Euler(_yDeg, _xDeg, 0);

        _rotation = _desiredRotation;
        transform.rotation = _rotation;
    }

    public void PanningControl(Vector3 _worldDelta, float _panSpeed)
    {
        //grab the rotation of the camera so we can move in a psuedo local XY space
        _target.rotation = transform.rotation;
        _target.Translate(Vector3.right * -_worldDelta.x * _panSpeed);
        _target.Translate(transform.up * -_worldDelta.y * _panSpeed, Space.World);
        _toTargetPos = _target.transform.position;
    }

    public void ZoomControl(float _pinchScale)
    {
        _desiredDistance -= _pinchScale;
        _desiredDistance = Mathf.Clamp(_desiredDistance, float.MinValue, _maxDistance);

        if (_desiredDistance < _minDistance)
        {
            float toDistance = Mathf.Abs(_desiredDistance - _minDistance);
            Vector3 toDirection = (_target.position - transform.position).normalized;

            _desiredDistance = _minDistance;
            _toTargetPos = _target.position + (toDirection * toDistance);
        }
    }

    private void HideShowChart()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        int layerMask = 1 << LayerMask.NameToLayer("Tendons");
        layerMask |= 1 << LayerMask.NameToLayer("TlpAssets");
        layerMask |= 1 << LayerMask.NameToLayer("Risers");

        _targetIndex = -1;
        if (Physics.Raycast(ray, out hit, float.MaxValue, layerMask))
        {
            GameObject go = hit.collider.gameObject;

            if (go.layer == LayerMask.NameToLayer("Tendons"))
            {
                OnShowGraph?.Invoke("Tendons");
            }
            else if (go.layer == LayerMask.NameToLayer("Risers"))
            {
                OnShowGraph?.Invoke("Risers");
            }
            else if (go.layer == LayerMask.NameToLayer("TlpAssets"))
            {
                OnShowGraph?.Invoke(go.name);
                for (int i = 0; i < _assetsTransform.Length; i++)
                {
                    if (_assetsTransform[i].name == go.name)
                    {
                        _targetIndex = i;
                        _structureMarkersRect[i].gameObject.SetActive(false); // Marker Need to Dynamic, Turned off by Walid
                        break;
                    }
                }
            }
        }
        else
        {
            OnShowGraph?.Invoke("");
        }
    }

    private static float ClampAngle(float angle, float min, float max)
    {
        if (angle < -360)
            angle += 360;
        if (angle > 360)
            angle -= 360;

        return Mathf.Clamp(angle, min, max);
    }
    
    private bool isBeingDragged = false;
    public bool IsBeingDragged()
    {
        return isBeingDragged;
    }
 
    public void OnEndDrag()
    {
        isBeingDragged = false;
    }
 
    public void OnBeginDrag()
    {
        isBeingDragged = true;
    }
    
}